"use strict"
const exec = require("./exec");
module.exports = async function () {
    await exec("lerna bootstrap", { echo: true });
    await exec("lerna run compile", { echo: true });
}
