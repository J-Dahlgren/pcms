const exec = require("./exec");
const compile = require("./compile");
(async function () {    
    await compile();
    await exec("cd projects/ui/ && ng build --prod", { echo: true });
})();