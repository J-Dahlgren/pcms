module.exports = {
    moduleFileExtensions: ["ts", "js"],
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.ts$',
    testPathIgnorePatterns: [
        '/testData/',
        '/node_modules/',
        "projects/ui/",
        '(/__tests__/.*|(\\.|/)(test|spec))\\.d\.ts$'

    ],
    preset: 'ts-jest',
    globals: {
        "ts-jest": {
            tsConfig: 'tsconfig.base.json',
            diagnostics: false
        },


    },
    verbose: true,
    collectCoverage: true,
    collectCoverageFrom: [
        "**/*.ts",
        "!projects/server/src/server.ts",
        "!projects/ui/**/*.ts",
        "!**/*.d.ts",
    ]
}