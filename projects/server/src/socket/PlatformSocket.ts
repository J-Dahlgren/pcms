import { SocketServerRoomDataGroup, AuthFunction } from "./ServerDataGroup";
import { PlatformData, ServerRxPlatformStream, ServerTxPlatformStream } from "shared";

export type PlatformDataGroup = SocketServerRoomDataGroup<PlatformData, ServerRxPlatformStream, ServerTxPlatformStream>
export class PlatformSocket extends SocketServerRoomDataGroup<PlatformData, ServerRxPlatformStream, ServerTxPlatformStream>  {

    constructor(PlatformId: string, server: SocketIO.Server, authFunc?: AuthFunction | undefined) {
        super(PlatformId, new PlatformData(), new ServerRxPlatformStream(), new ServerTxPlatformStream(), server, authFunc);
    }
}