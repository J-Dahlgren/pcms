import { Router } from "express";
import { RouterSettings } from "./routerSettings";
import { HttpJsonResponse, HttpJsonResponseStatus } from "shared";
import { IController } from "../controllers/IController";
interface IKeyedObject {
    [key: string]: any;
}
export function CreateRouter<T extends IKeyedObject>(Settings: RouterSettings, controller: IController<T>): Router {
    const router = Router();
    if (Settings.Create) {
        const c = Settings.Create;
        router.post("/", (req, res) => {
            try {
                const C = controller.Create(req.body as T);
                if (C) {
                    res.json(CreateResponse<T>("SUCCESS", C, null));
                } else {
                    res.json(CreateResponse<null>("ERROR", null, `Could not create ${Settings.Name}`));
                }
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
    }
    if (Settings.Read) {
        router.get("/", (req, res) => {
            try {
                const C = controller.ReadAll();
                const queryParams = req.query;
                let data = C;
                if (queryParams) {
                    data = C.filter((t: T) => {
                        for (const key of Object.keys(queryParams)) {
                            // tslint:disable-next-line: triple-equals
                            if (t[key] != queryParams[key]) { // We don't want strict equal since all queryparams will be string values
                                return false;
                            }
                        }
                        return true;
                    });
                }
                res.json(CreateResponse<T[]>("SUCCESS", data, null));
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
        router.get("/:id", (req, res) => {
            try {
                const id = +req.params.id as number;
                const C = controller.Read(id);
                if (C) {
                    res.json(CreateResponse<T>("SUCCESS", C, null));
                } else {
                    res.json(CreateResponse<null>("ERROR", null, `Could not find ${Settings.Name} with Id ${id}`));
                }
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
    }
    if (Settings.Update) {
        const u = Settings.Update;
        router.put("/", (req, res) => {
            try {
                const Obj = req.body as T & LokiObj;
                const Upd = controller.Update(Obj);
                if (Upd) {
                    res.json(CreateResponse<T>("SUCCESS", Upd, null));
                } else {
                    res.json(CreateResponse<null>("ERROR", null, `Could not update ${Settings.Name} with Id ${Obj.$loki}`));
                }
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
        router.patch("/:id", (req, res) => {
            try {
                const Id: number = +req.params.id;
                const Obj = req.body as T & LokiObj;
                const Upd = controller.Patch(Id, Obj);
                if (Upd) {
                    res.json(CreateResponse<T>("SUCCESS", Upd, null));
                } else {
                    res.json(CreateResponse<null>("ERROR", null, `Could not update ${Settings.Name} with Id ${Obj.$loki}`));
                }
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
    }
    if (Settings.Delete) {
        router.delete("/:id", (req, res) => {
            try {
                const Id: number = +req.params.id;
                if (controller.Delete(Id)) {
                    res.json(CreateResponse<T>("SUCCESS", null, null));
                } else {
                    res.json(CreateResponse<null>("ERROR", null, `Could not delete ${Settings.Name} with Id ${Id}`));
                }
            } catch (error) {
                res.json(CreateResponse<null>("ERROR", null, `${error.message}`));
            }
        });
    }
    return router;
}
export function CreateResponse<T>(OperationStatus: HttpJsonResponseStatus, Data: T | null, Message: string | null): HttpJsonResponse<T | null> {
    return { Data, Message, Status: OperationStatus };
}