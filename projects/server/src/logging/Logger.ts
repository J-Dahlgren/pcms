import chalk from "chalk";
// tslint:disable-next-line: import-name
import momentImported from "moment";
const moment = momentImported;
export type LogLevel = "ERROR" | "WARNING" | "INFO" | "DEBUG" | "OFF"

type LogLevelData = [number, chalk.Chalk];
const LoggingData: { [key in LogLevel]: LogLevelData } = {
    ERROR: [0, chalk.red],
    WARNING: [1, chalk.yellow],
    INFO: [2, chalk.blueBright],
    DEBUG: [3, chalk.magenta],
    OFF: [-1, chalk.black]
}
class Logger {
    public LogLevel: LogLevel = "OFF";
    public debug(...args: any[]) {
        this.Line("DEBUG", ...args);
    }
    public info(...args: any[]) {
        this.Line("INFO", ...args);
    }
    public warn(...args: any[]) {
        this.Line("WARNING", ...args);
    }
    public error(...args: any[]) {
        this.Line("ERROR", ...args);
    }
    public Line(level: LogLevel, ...args: any[]) {
        const LogData = LoggingData[level];

        if (LogData[0] <= LoggingData[this.LogLevel][0]) {
            console.log(this.Timestamp, LogData[1](`${level}\t`), ...args);
        }
    }
    private get Timestamp(): string {
        return chalk.green(moment(Date.now()).format("YYYY-MM-DD HH:mm:ss.SSS"));
    }
}
export const Log = new Logger();