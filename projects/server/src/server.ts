import express from "express";
import * as path from "path";
import { config } from "dotenv";
import { InitializeApplication } from "./app";
import * as http from "http";
import SocketServer from "socket.io";
import "reflect-metadata";
import { container } from "tsyringe";
import LowDB from "lowdb";

import FileSync from "lowdb/adapters/FileSync";

import { InitDatabase } from "./database";
import shortid from "shortid";
import { Attempt, Athlete, LiftStatus, AthleteData, Entry, PlatformData, ApplicationData } from "shared";
import { PcmsSchema } from "./database/schema";
import { InitLoki, LokiDB } from "./database/loki";
import { SessionManager } from "./session";
import { PlatformSocketFactoryFn } from "./socket/PlatformSocketFactory";
import { PlatformSocket } from "./socket/PlatformSocket";
import { Log } from "./logging/Logger";
import { SocketServerDataGroup } from "./socket/ServerDataGroup";
import { platform } from "os";


const lowdb = LowDB(new FileSync<{ Athletes: Entry<AthleteData>[] }>("DB/AthleteData.json", { defaultValue: { Athletes: [] } }));
config();
Log.LogLevel = process.env.NODE_ENV === "dev" ? "DEBUG" : "INFO";
InitLoki("DB/loki.json")
    .then(rawLokiDB => {
        Log.info(`Running in ${process.env.NODE_ENV} mode`);
        Log.info(`Log Level: ${Log.LogLevel}`);
        const port = process.env.SERVER_PORT || 8080;
        const db = new LokiDB(rawLokiDB);

        const App = InitializeApplication(db);
        const server = http.createServer(App);

        server.listen(port, () => {
            Log.info(`Application listening on port ${port}`);
        });
        const Socket = SocketServer();
        Socket.listen(server);
        Socket.on("connection", socket => Log.debug(`Client ${socket.id} connected`));
        const AppInfo = new SocketServerDataGroup(Socket, new ApplicationData());

        db.Platforms.Stream.subscribe(platforms => AppInfo.PropGroup.Platforms.Value = platforms);
        const factoryFn: PlatformSocketFactoryFn = Room => new PlatformSocket(Room, Socket);
        const mngr = new SessionManager(db, factoryFn);

        if (process.env.NODE_ENV === "prod") { // Serve static files from www-folder  
            App.use(express.static("www"));
            App.get("*", (req, res) => {
                res.sendFile(path.join(process.cwd(), "www/index.html"));
            });
        }
    })
    .catch(err => Log.error(err));
