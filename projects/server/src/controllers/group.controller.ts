import { IController } from "./IController";
import { LOKI, AthleteData, Athlete } from "shared";
import { Group } from "shared";
import { AbstractController, OperationConstraints } from "./abstract.controller";
import { LokiCollection } from "../database";

export class GroupController extends AbstractController<Group>{
    constructor(Groups: LokiCollection<Group>, protected Athletes: LokiCollection<Athlete>) {
        super(Groups);
        this.Constraints = {
            CanDelete: entry => {                                
                if (this.Athletes.Collection.findOne({ GroupName: { $eq: entry.Name } })) {
                    return `Can't delete group ${entry.Name} when an athlete is assigned to it`;
                }
                return false;
            }
        };
    }
}