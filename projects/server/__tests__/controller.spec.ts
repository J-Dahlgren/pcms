import { InitLoki, LokiCollection, GroupController, AthleteController, PlatformController } from "../src";
import { Group, Athlete, LiftStatus, Platform, AthleteData } from "shared";
const GroupName = "TestGroup";
const PlatformName = "TestPlatform"
const MockAthleteData: Athlete =
{
    Firstname: "",
    Lastname: "",
    Gender: "M",
    Lot: 1,
    GroupName,
    Lifts: {
        Squat: [],
        Bench: [],
        Deadlift: []
    }
}

describe("Group Controller", () => {
    it("Blocks deletion when an athlete is assigned to the group", async () => {
        const db = await InitLoki("", false);
        const group = new LokiCollection<Group>(db, "group");
        const athlete = new LokiCollection<Athlete>(db, "athlete");
        const insertedGroup = group.Collection.insertOne({ Name: GroupName });
        const insertedAthlete = athlete.Collection.insertOne(MockAthleteData);
        const groupController = new GroupController(group, athlete);
        const athleteController = new AthleteController(athlete);
        expect(group.data.length).toEqual(1);

        expect(() => groupController.Delete(insertedGroup.$loki)).toThrow();
        expect(group.data.length).toEqual(1);
        athleteController.Patch(insertedAthlete.$loki, { GroupName: Date.now.toString() });
        expect(() => groupController.Delete(insertedGroup.$loki)).not.toThrow();
        expect(group.data.length).toEqual(0);

    });
    it("Returns false when deleting non-existing entry", async () => {
        const db = await InitLoki("", false);
        const group = new LokiCollection<Group>(db, "group");
        const athlete = new LokiCollection<Athlete>(db, "athlete");
        const groupController = new GroupController(group, athlete);
        expect(groupController.Delete(1)).toEqual(false);
    });
});
describe("Platform Controller", () => {
    it("Blocks deletion when a group is assigned to the platform", async () => {
        const db = await InitLoki("", false);
        const platform = new LokiCollection<Platform>(db, "platform");
        const group = new LokiCollection<Group>(db, "group");
        const athlete = new LokiCollection<Athlete>(db, "athlete");

        const insertedPlatform = platform.Collection.insertOne({ Name: PlatformName });
        const insertedGroup = group.Collection.insertOne({ Name: GroupName, PlatformName });
        const platformController = new PlatformController(platform, group);
        const groupController = new GroupController(group, athlete);
        expect(platform.data.length).toEqual(1);

        expect(() => platformController.Delete(insertedPlatform.$loki)).toThrow();
        expect(platform.data.length).toEqual(1);
        groupController.Patch(insertedGroup.$loki, { PlatformName: Date.now.toString() });
        expect(() => platformController.Delete(insertedPlatform.$loki)).toThrow();
        expect(platform.data.length).toEqual(1);

    });
    it("Returns false when deleting non-existing entry", async () => {
        const db = await InitLoki("", false);
        const platform = new LokiCollection<Platform>(db, "platform");
        const group = new LokiCollection<Group>(db, "group");
        const platformController = new PlatformController(platform, group);
        expect(platformController.Delete(1)).toEqual(false);
    });
});