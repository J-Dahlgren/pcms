import { Component } from "@angular/core";
import { NavItem } from "./menu-list-item/nav-item";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent {
    title = "Powerlift CMS";
    navItems: NavItem[] = [];

}
