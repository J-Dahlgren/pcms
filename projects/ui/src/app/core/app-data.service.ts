import { Injectable } from "@angular/core";
import { EnvironmentService } from "./environment.service";
import { BehaviorSubject } from "rxjs";
import { ApplicationData } from "shared";
import { SocketService } from "./socket.service";
import { SocketClientDataGroup } from "./client-data-group";
@Injectable({
    providedIn: "root"
})
export class AppDataService {

    private appData: SocketClientDataGroup<ApplicationData>;
    public get data() {
        return this.appData.PropGroup;
    }
    constructor(socketService: SocketService) {
        this.appData = new SocketClientDataGroup(socketService.Socket, new ApplicationData());
        this.data.Platforms.Stream.subscribe(s => console.log(s));
    }
}
