import { Injectable } from "@angular/core";
import { LOKI, HttpJsonResponse } from "shared";
import { EnvironmentService } from "./environment.service";
import { HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { delay, tap, catchError, map, finalize } from "rxjs/operators";
import { LoadService } from "./load.service";
@Injectable({
    providedIn: "root",
})
export class ApiService {

    constructor(
        private envService: EnvironmentService,
        private httpClient: HttpClient,
        private loadService: LoadService,
        private snackBar: MatSnackBar
    ) {

    }
    Create<T>(Endpoint: string, Data: T) {
        this.loadService.ShowLoading();
        return this.httpClient.post<HttpJsonResponse<LOKI<T>>>(this.PrepEndpoint(Endpoint), Data).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS" && value.Data) {
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to create";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                if (error) {
                    throw error;
                } else {
                    throw new Error("Unknown error occurred");
                }

            })
        );
    }
    Read<T>(Endpoint: string, Id: number) {
        this.loadService.ShowLoading();
        return this.httpClient.get<HttpJsonResponse<LOKI<T>>>(this.PrepEndpoint(`${Endpoint}/${Id}`)).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS" && value.Data) {
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to read";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                if (error) {
                    throw error;
                } else {
                    throw new Error("Unknown error occurred");
                }
            })
        );
    }
    ReadAll<T>(Endpoint: string) {
        this.loadService.ShowLoading();
        return this.httpClient.get<HttpJsonResponse<LOKI<T>[]>>(this.PrepEndpoint(Endpoint)).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS" && value.Data) {
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to read all";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                if (error) {
                    throw error;
                } else {
                    throw new Error("Unknown error occurred");
                }
            })
        );
    }
    Patch<T>(Endpoint: string, Id: number, Data: Partial<T>) {
        this.loadService.ShowLoading();
        return this.httpClient.patch<HttpJsonResponse<LOKI<T>>>(this.PrepEndpoint(`${Endpoint}/${Id}`), Data).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS" && value.Data) {
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to update";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                throw error;
            })
        );
    }
    Update<T>(Endpoint: string, Data: LOKI<T>) {
        this.loadService.ShowLoading();
        return this.httpClient.put<HttpJsonResponse<LOKI<T>>>(this.PrepEndpoint(Endpoint), Data).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS" && value.Data) {
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to update";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                throw error;
            })
        );
    }
    // Upsert<T>(Endpoint: string, Data: T & { "$loki"?: number }) {
    //     if (Data.$loki) {
    //         return this.Update<T>(Endpoint, Data);
    //     }
    //     return this.Create<T>(Endpoint, Data);
    // }
    Delete(Endpoint: string, Id: number) {
        this.loadService.ShowLoading();
        return this.httpClient.delete<HttpJsonResponse<null>>(this.PrepEndpoint(`${Endpoint}/${Id}`)).pipe(
            // delay(this.Delay),
            finalize(() => this.loadService.HideLoading()),
            tap((value) => {
                const data = value.Data;
                if (value.Status === "SUCCESS") {
                    this.snackBar.open("Succesfully deleted item!", undefined, { duration: 2500, panelClass: "snack-bar-success" });
                    return data;
                }
                const msg = value.Message ? value.Message : "Failed to delete";
                throw new Error(`${value.Status}: ${msg}`);
            }),
            catchError(error => {
                this.ErrorHandler(error);
                throw error;
            })
        );
    }
    private ErrorHandler(error: any) {
        const msg = error && error.message ? error.message : "An error occurred";
        this.snackBar.open(msg, undefined, { duration: 2500, panelClass: "snack-bar-failed" });
    }
    private PrepEndpoint(Endpoint: string) {
        return this.envService.production ? `http://localhost:8080/api/${Endpoint}` : `/api/${Endpoint}`;
    }
}
