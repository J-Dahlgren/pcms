import { IEnvironment } from "./IEnvironment";

export const environment: IEnvironment = {
    production: true,
    socketPort: 8080
};
