import { ReactiveProperty, NullReactiveProperty } from "../src";
describe("ReactiveProperty", () => {
    it("Updates Value", () => {
        const rp = new ReactiveProperty<number>(0);
        expect(rp.Value).toEqual(0);
        rp.Value = 1;
        expect(rp.Value).toEqual(1);
    });
    it("Emits Values", done => {
        const rp = new ReactiveProperty<number>(1);
        rp.Stream.subscribe(val => {
            expect(val).toEqual(1);
            done();
        })
    });
    test("NullReactiveProperty inital value works",()=> {
        let rp = new NullReactiveProperty<string>();
        expect(rp.Value).toBeNull();
        rp.Value = "X";
        expect(rp.Value).toEqual("X");
        rp = new NullReactiveProperty("X");
        expect(rp.Value).toEqual("X");
    })

});