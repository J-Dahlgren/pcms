import { LiftingOrder } from "../src";
import { LiftOrderData } from "./testData/athletes";


describe("Lifting Order", () => {
    it("Calculates the correct order", () => {
        const lots = LiftingOrder(LiftOrderData).map(a => a.Lot);
        expect(lots).toEqual([2, 1, 5, 3, 4])
    })
});