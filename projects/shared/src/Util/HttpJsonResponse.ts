export type HttpJsonResponseStatus = "SUCCESS" | "ERROR";

export interface HttpJsonResponse<T> {
    Status: HttpJsonResponseStatus;
    Data: T | null;
    Message: string | null;
}
