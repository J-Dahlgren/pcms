export type Optional<M, O> = (M & O) | M;
