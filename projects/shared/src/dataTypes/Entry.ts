export type ID = { id: string };
export type Entry<T extends object> = T & ID;
