import { ReactiveProperty, NullReactiveProperty } from "../Util/ReactiveProperty";
import { Subject} from "rxjs";
const JOIN = "JOIN_ROOM";
const LEAVE = "LEAVE_ROOM";
export interface SyncedData<T> {
    Event: string;
    Data: ReactiveProperty<T>;
}
export interface ReactivePropertyGroup {
    [key: string]: ReactiveProperty<any>;
}
export interface StreamGroup {
    [key: string]: Subject<any>
}
export const CLIENT_SYNC_REQUEST = "CLIENT_SYNC_REQUEST";
export const CLIENT_AUTH_REQUEST = "CLIENT_AUTH_REQUEST";
export interface RoomData<T = any> {
    Room: string;
    Data: T
}
export abstract class DataGroup<T extends ReactivePropertyGroup, RxStream extends StreamGroup = {}, TxStream extends StreamGroup = {}>{
    constructor(private syncData: T, private rxStream: RxStream, private txStream: TxStream) {

    }
    public get PropGroup(): T {
        return this.syncData;
    }
    public get RxStream(): RxStream {
        return this.rxStream;
    }
    public get TxStream(): TxStream {
        return this.txStream;
    }
}
export abstract class DataGroupRoom<Rpg extends ReactivePropertyGroup, RxStream extends StreamGroup = {}, TxStream extends StreamGroup = {}> extends DataGroup<Rpg, RxStream, TxStream>{
    constructor(protected room: string, syncData: Rpg, rxStream: RxStream, txStream: TxStream) {
        super(syncData, rxStream, txStream);
    }
    public get Room(): string {
        return this.room;
    }
    public set Room(v: string) {
        this.room = v;
    }
    public CreateRoomData(value: any): RoomData {
        return {
            Room: this.Room,
            Data: value
        }
    }
}


